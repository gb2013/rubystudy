def readFile(fileName)
  f=File.open(fileName)
  txt  =''
  f.each_line do |line|
             txt+=line
  end
  puts txt
end

readFile(File.dirname(__FILE__) +"/data.txt")
t = Thread.new do # Run this block in a new thread
  File.read(File.dirname(__FILE__) +"/data.txt") # Read a file in the background
end # File contents available as thread value

